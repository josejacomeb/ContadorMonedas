#!/usr/bin/env python3
# -*- coding: utf-8 -*-

import cv2 #Libreria d Vision Artificial
import argparse  # Obtener valores desde consola
import logging  # Libreria logs
import numpy as np  # Liberias de Numpy
import json  # Leer las configuraciones
import serial  # Enviar datos a traves del puerto serial de la PC


def build_argparser():
    """
    Verifica los argumentos de entrada del programa
    :return: un objeto con la línea de comandos
    """
    parser = argparse.ArgumentParser(description="Programa contador de monedas") #Añado que el usuario 
    parser.add_argument("-i", '--entrada', required=True, help="Entrada, número entero de la cámara o ruta al archivo de video") #Define el número de cámara del equipo anfitrión o la dirección del archivo de video para procesar
    parser.add_argument("-j", '--json', help="Ubicación del archivo configuraciones.json",
                        default="configuraciones.json") #Define la ruta del archivo configuraciones.json, archivo donde se puede configurar la detección de más monedas o enviar diferentes parámetros al puerto serial,
                                                        #por defecto es configuraciones.json que esta en la misma carpeta del codigo

    return parser #Enviar el gestor de argumentos al programa principal


def main():
    """
    Programa principal
    :return: Error si es que lo hay
    """
    args = build_argparser().parse_args() #Crea un gestor de argumentos y lo recibe
    cap = cv2.VideoCapture(args.entrada) #Crear un objeto que leera de la camara o del archivo de video
    if not cap.isOpened(): #si falla la carga de la camara o video
        print("No se puede abrir la entrada {}, por favor, revisela".format(args.entrada))
        return -1 #Termina la ejecucion con error
    activado_serial = False #Bandera para inicializar si el puerto serial está activado
    with open(args.json) as f: 
        configuraciones = json.load(f) #Carga los valores de configuracion del archivo configuraciones.json
    try: #Intenta abrir el puerto serial, sino retorna error
        ser = serial.Serial(configuraciones["puerto_serial"], configuraciones["velocidad_bauds"]) #Carga el puerto serial con las configuracione de serial
        if ser.isOpen(): #Si se logra abrir, se enviará datos
            activado_serial = True
    except:
        print("No se puede utilizar el puerto serial, verifique el puerto en el archivo configuraciones.json")

    kernel = cv2.getStructuringElement(cv2.MORPH_ELLIPSE, ( 5, 5)) #Parametros del kernel para la Erosion y Dilatacion de la imagen
    tolerancia = configuraciones["tolerancia_cm"]  # Tolerancia en mm
    font = cv2.FONT_HERSHEY_SIMPLEX  # Tipo d fuente
    fontScale = 1  # Escala de la fuente
    color = (255, 0, 0)  # Color en BGR
    thickness = 2  # Ancho de la linea en px
    caracter_enviar = "0".encode('utf-8') #El valor del caracter se debe codificar en utf-8
    anterior_moneda_detectada = "ninguna" #Inicializacion bandera de anterior_moneda_detectada
    actual_moneda_detectada = "ninguna" #Inicializacion bandera de actual_moneda_detectada

    max_tiempo_mostrar = 30 #Maximo tiempo para mostrar una imagen, en cuadros(30 cuadross es 1s)
    contador_tiempo_mostrar = 0 #Contador de cuanto cuadros han pasado, desde que sse mostrro un mensaje
    while True:
        ret, img = cap.read() #Leer uma imagen de la cámara o video
        if img is None: #si no hay imagen, cerrar el programa y el puerto ssesrial
            print('No hay imagen para abrir!')
            if activado_serial:
                ser.close()
            return -1
        alto, ancho = img.shape[0:2] #Obtener el ancho y alto de la imagen en pixeles
        ratio = configuraciones["distancia_real_y_cm"] / alto #Obtiene un razon verticial entre la proporcion real y el alto de la imagen en pxx
        img_recortada = img[int(alto * configuraciones["zona_recorte"]["prop_y_min"]):int(
            alto * configuraciones["zona_recorte"]["prop_y_max"]),
                        int(ancho * configuraciones["zona_recorte"]["prop_x_min"]):int(
                            ancho * configuraciones["zona_recorte"]["prop_x_max"])]     #Recortar la imagen al tamaño definido en la zona de recorte
        imagen_gris = cv2.cvtColor(img_recortada, cv2.COLOR_BGR2GRAY) #Transformar la imagen a gris
        cv2.normalize(imagen_gris, imagen_gris, 0, 255, cv2.NORM_MINMAX) #Normaliza la imagen, para obtener mejorar los efectos de la iluminacion
        imagen_gris_desenfocada = cv2.medianBlur(imagen_gris, 3) #Aplica el filtro de desenfoque mediano para suavizar los contornos
        ret2, otsu_th = cv2.threshold(imagen_gris_desenfocada, 0, 255, cv2.THRESH_BINARY + cv2.THRESH_OTSU) #Aplico umbralizacion para separar los elementos negros de los blancos
        otsu_th = cv2.erode(otsu_th, kernel, iterations=3) #Erosiono la imagen para eliminar puntos de luz y otros objetos que no son de interes
        otsu_th = cv2.dilate(otsu_th, kernel, iterations=3) #Dilato la imagen para fortalecer los objeto detectados

        # Enmascarar imagen
        imagen_gris = imagen_gris_desenfocada & otsu_th
        minimoRadiopx = int(configuraciones["minimo_radio_cm"] * alto / configuraciones["distancia_real_y_cm"]) #Obtengo el minimo radio en px que vamos a detectar
        maximoRadiopx = int(configuraciones["maximo_radio_cm"] * alto / configuraciones["distancia_real_y_cm"]) #Obtengo el maximo radio en px que vamos a detectar
        distanciaCentrospx = int(
            configuraciones["distancia_centros_cm"] * alto / configuraciones["distancia_real_y_cm"]) #Calculo la mínima distancia entre centros que puede haber entre las monedas, para evitar falsas detecciones
        circulos_detectados = cv2.HoughCircles(imagen_gris,
                                            cv2.HOUGH_GRADIENT, 1, distanciaCentrospx,
                                            param1=configuraciones["param1"], param2=configuraciones["param2"],
                                            minRadius=minimoRadiopx, maxRadius=maximoRadiopx) #Aplico la detección de Circulos a través del algoritmo HoughCircles 
        # Itero sobre los circulos detectados, para reconocer a que moneedas pertenecen
        if circulos_detectados is not None: #Si hay un círculo detectado, prosigo con el cáluclo

            # Convierto los parámetros del círculo a centro en x, centro en y y radio (a,b) y r.
            circulos_detectados = np.uint16(np.around(circulos_detectados))
            #Itero sobre los puntos detectados
            for pt in circulos_detectados[0, :]:
                a, b, r = pt[0], pt[1], pt[2]
                #Dibujo la circunferencia del círculo
                cv2.circle(img_recortada, (a, b), r, (0, 255, 0), 2)
                diametro_moneda = 2 * r * ratio #Obtengo el diamtro da la moneda en cm
                # Dibujo un pequeño circulo para que se muestre le centro
                cv2.circle(img_recortada, (a, b), 1, (0, 0, 255), 3)
                tipo_moneda = None #Inicializacion de la variable tipo de moneda

                for idx, llave in enumerate(configuraciones["monedas"]): #Itero sobre mis configuraciones de monedas disponibles y veo a cual pertenece
                    diametro_moneda_registrado = configuraciones["monedas"][llave]["tamano_cm"] #Leo el diametro de la moneda
                    if (diametro_moneda_registrado + tolerancia > diametro_moneda) & \
                            (diametro_moneda_registrado - tolerancia < diametro_moneda): #Aplico la tolerancia establecida y veo si se encuentra en el rango
                        tipo_moneda = configuraciones["monedas"][llave] #Si esta en rango, el tipo de moneda es registrado
                        tipo_moneda["centro"] = (a, b)
                        tipo_moneda["radio"] = r
                        actual_moneda_detectada = tipo_moneda["nombre"]
                        contador_tiempo_mostrar = 0

                if tipo_moneda: #Si se ha detectado una moneda
                    img_recortada = cv2.putText(img_recortada, "{}".format(tipo_moneda["nombre"]), #Muestro el texto del nombre d la moneda
                                                tipo_moneda["centro"], font, fontScale, tipo_moneda["color"],
                                                thickness, cv2.LINE_AA)
                    cv2.circle(img_recortada, tipo_moneda["centro"], tipo_moneda["radio"], tipo_moneda["color"], 10) #hago un circulo más grueso sobre la moneda
                    if activado_serial: #Si le puerto serial ha sido exitosamente activado, envio el dato
                        if caracter_enviar != tipo_moneda["caracter_enviar"]:  # Enviar una sola vez
                            ser.write(tipo_moneda["caracter_enviar"].encode('utf-8'))
                            caracter_enviar = tipo_moneda["caracter_enviar"] 
                img_recortada = cv2.putText(img_recortada, "D: {:.2f} cm".format(diametro_moneda),
                                            (int(abs(int(a) - int(r))), int(abs(int(b) - int(r)))), font,
                                            fontScale, (0, 120, 255), thickness, cv2.LINE_AA) #Muestro el diametro de la moneda
        if contador_tiempo_mostrar < max_tiempo_mostrar: #Muestro por un periodo pequeño de tiempo
            img_recortada = cv2.putText(img_recortada, "Moneda detectada: {}".format(actual_moneda_detectada),
                                        (10,25), font, fontScale, (255, 0, 255), thickness, cv2.LINE_AA)
            contador_tiempo_mostrar += 1
        cv2.imshow("Gris", imagen_gris) #Muestro las imagenes procesadas
        cv2.imshow("Otsu", otsu_th)
        cv2.imshow("Original", img)
        cv2.imshow("Monedas detectadas", img_recortada)

        c = cv2.waitKey(30)
        if c == 27: #Presiono escape para salir
            break
    if activado_serial:
        ser.close()


if __name__ == '__main__':
    main()
