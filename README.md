# ContadorMonedas

Programa configurable parra contar de monedas de distintos tipos en un fondo negro. 

## Instalación

### PIP
Con PIP instalado, basta  con ejecutar el siguiente comando desde la carpeta del archivo

```bash
pip install -r requisitos.txt
```

### Conda
Con Anaconda, igualmente se usa el archivo de creación del ambiente de trabajo

```bash
conda env create -f environment.yml
```

Luego se activa, con el siguiente comando y está habilitado para correr.


```bash
conda activate ContadorMonedas
```

## Uso
El nombre del script principal es `main.py`. 
Este se puede ejecutar de la siguiente forma.

```bash
python3 main.py [-h] -i ENTRADA [-j JSON]
```
### Configuración inicial

El archivo `configuraciones.json` es el archivo donde el usuario define las configuraciones de la detección y del Arduino. Los parámetros son los siguientes:
+ `distancia_real_y_cm`: El programa internamente transforma los pixeles de la imagen a cm, esa conversión necesita el valor real del prototipo y cambia con la altura. Para medirlo, basta con usar un programa para ver la salida de la cámara(puede ser VLC), alinear una regla con el eje vertical de la imagen y guardar ese valo en cmr.  **Configurar al principio**
+ `puerto_serial`: Dirección del Puerto Serial donde está conectado el Arduino. **Configurar al principio**
+ `minimo_radio_cm`: Mínimo radio para detectar en cm, ayuda a evitar falsas detecciones, de preferencia ponerle un 90% del tamaño de la moneda más pequeña. *Por defecto: 1*
+ `maximo_radio_cm`: Máximo radio para detectar en cm, ayuda a evitar falsas detecciones, de preferencia ponerle un 110% del tamaño de la moneda más grande. *Por defecto: 8*
+ `distancia_centros_cm`: Mínima distancia en cm que debe haber entre los centros de una detección, ayuda a evitar falsas detecciones, de preferencia ponerle el tamaño de la moneda más pequeña. *Por defecto: 1*
+ `zona_recorte`: Proporciones que ayudan a encuadrar la imagen de las monedas, para un procesamiento más rápido. El marco de referencia de las proporciones se toman desde la esquina superior izquierda de la imagen. Y como un rectángulo, se trata de definir la esquina mínima y la esquina máxima, entonces los parámetros son:
![alt text](FormaContar.svg "Forma contar")
  * `prop_x_min`: Proporción de la esquina más cercana al eje x, vease E1 *Por defecto: 0.3*
  * `prop_y_min`: Proporción de la esquina más cercana al eje y, vease E1  *Por defecto: 0.1*
  * `prop_x_max`: Proporción de la esquina más lejana al eje x, vease E2 *Por defecto: 0.9*
  * `prop_y_max`: Proporción de la esquina más lejano al eje y, vease E2 *Por defecto: 0.9*
+ `param1` y `param2`: Parámetros de detección de la Función de Detectar Círculos. *Por defecto: 200, 40* Para más información, vease [Parametros Hough Circle](https://docs.opencv.org/3.4/dd/d1a/group__imgproc__feature.html#ga47849c3be0d0406ad3ca45db65a25d2d)
+ `velocidad_bauds`: Velocidad de comunicación entre el PC y el Arduino, asegurarse de establecer la misma en el Arduino *Por defecto: 115200*
+ `tolerancia_cm`: Tolerancia en cm para establecer un rango de valores en el que el tamaño detectado puede estar, puede ser el 25% del tamaño de la moneáda ms pequeña. *Por defecto: 0.12*
+ `monedas`: Definido como un diccionario de Python, sin cambiar el código, permite reconocer monedas de distintos tamaños, asignarle un nombre para mostrar en la pantalla con la variable `nombre`, se define midiendo el tamaño de la moneda y poniendolo en la varaible `tamano_cm`. La variable `caracter_enviar` es un solo caracter que va a ser enviado al Arduino. La variable `color`
### Argumentos
Los argumentos  disponibles son los siguientes. 

```bash
  -h, --help            muestra el mensaje de ayuda y sale
  
  -i ENTRADA, --entrada ENTRADA
                        Entrada, Entrada, número entero de la cámara o ruta al archivo de video
                        
  -j JSON, --json JSON  Ubicación del archivo configuraciones.json
```

Ejemplo de uso

```bash
python3 main.py -i 0
```

*Notas*: El número de la cámara empieza en 0,1,2, ..., el nombre del archivo de video pueder ser: `C:/myruta/../miarchivo.mp4`.

Se puede especificar más de un archivo de configuración.




