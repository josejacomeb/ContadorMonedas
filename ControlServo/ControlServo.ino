#include <Servo.h>

int PIN_SERVO = 9; // Pin donde estará colocado el servo

Servo myservo;  // crea el objeto servo

int pos = 0;    // posicion del servo
int pos_izquierda = 120; //Posicion que llegará el servo a la izquierda
int pos_derecha = 70; //Posssición que llegará el servo a la derecha
int centro = 90; //Posición centro
int tiempo_espera_servo = 15; //Tiempo de espera para cada ciclo del servo
char dato_izquierda = 'I'; //Caracter para mover izquierda 
char dato_derecha = 'D'; //Caracter mover derecha
char datorecibido = '0'; //Inicializacion dato 
void mover_servo_intervalo(int datodestino, int  datoinicio, int tiempoespera) { //Funcino para moveer el servo, con tiempo de espera
  if (datodestino > datoinicio) {
    for (pos = datoinicio; pos <= datodestino; pos += 1) //Aumente de a uno y mueva en sentido horario
    {
      myservo.write(pos);
      delay(15);
    }
  }
  else {
    for (pos = datoinicio; pos <= datodestino; pos -= 1) //Disminuyá de a uno y mueva en sentido antihorario
    {
      myservo.write(pos);
      delay(15);
    }
  }
}

void setup() {
  myservo.attach(PIN_SERVO);  // vincula el servo al pin digital 9
  Serial.begin(115200); //Inicia la conexión a la velocidad del PC
  myservo.write(pos); //Inicializa centro
}

void loop() {
  if (Serial.available() > 0) { //Lea si hay datos en el buffer
    datorecibido = Serial.read(); //Leer el caracter 
    if (datorecibido == dato_izquierda) { //Si el dato recibido, mueva a la izquierda
      mover_servo_intervalo(centro, pos_izquierda, tiempo_espera_servo); //Mover izquierda
      mover_servo_intervalo(pos_izquierda, centro, tiempo_espera_servo); //Volver al centro
    }
    else if (datorecibido == dato_derecha) {
      mover_servo_intervalo(centro, pos_derecha, tiempo_espera_servo); //Mover derecha
      mover_servo_intervalo(pos_derecha, centro, tiempo_espera_servo); //Volver al  centro
    }
  }
}
